<!-- writeme -->
Writeme
=======

Create and keep updated a README.md for your project, fetching details from composer.json and Git. Composer does not need to be installed to use this tool.

 * http://gitlab.com/drutopia/writeme
 * Documentation: https://docs.drutopia.org/
 * Issues: http://gitlab.com/drutopia/writeme/issues
 * Keywords: readme, composer, php, documentation, utility, script
 * Package name: drutopia/writeme


### Maintainers

 * Benjamin Melançon (mlncn) - http://agaric.coop/mlncn

### Requirements

 * php >=5.3.6


### License

[GPL-2.0+: GNU General Public License, version 2 or later](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

<!-- endwriteme -->

### Usage

```
php path/to/writeme/src/writeme.php
```

Or add an alias to the path to make it easy to use anytime you are next to a composer.json from which you would like to generate a README.md file.  For example, in [my .bashrc](https://gitlab.com/mlncn/scripts/blob/master/.bash_custom ) i have:

```
alias writeme='php ~/Projects/drutopia/writeme/src/writeme.php'
```


### Background

Inspired by but no longer containing any code from [docbloc](http://github.com/intrd/docbloc).
